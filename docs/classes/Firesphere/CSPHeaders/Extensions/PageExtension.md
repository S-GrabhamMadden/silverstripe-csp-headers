---
title: \Firesphere\CSPHeaders\Extensions\PageExtension
footer: false
---

# PageExtension

Class \Firesphere\CSPHeaders\Extensions\SiteTreeExtension



* Full name: `\Firesphere\CSPHeaders\Extensions\PageExtension`
* Parent class: [DataExtension](../../../../classes.md)



## Methods

### updateSettingsFields



```php
public PageExtension::updateSettingsFields(\SilverStripe\Forms\FieldList $fields): mixed
```








**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `fields` | **\SilverStripe\Forms\FieldList** |  |


**Return Value:**





---


---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)
