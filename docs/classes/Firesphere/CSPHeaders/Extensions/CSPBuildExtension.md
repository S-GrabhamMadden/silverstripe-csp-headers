---
title: \Firesphere\CSPHeaders\Extensions\CSPBuildExtension
footer: false
---

# CSPBuildExtension

Class \Firesphere\CSPHeaders\Extensions\CSPBuildExtension



* Full name: `\Firesphere\CSPHeaders\Extensions\CSPBuildExtension`
* Parent class: [Extension](../../../../classes.md)



## Methods

### afterCallActionHandler

Truncate the SRI table on build

```php
public CSPBuildExtension::afterCallActionHandler(): mixed
```









**Return Value:**





---


---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)
