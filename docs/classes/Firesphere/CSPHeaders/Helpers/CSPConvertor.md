---
title: \Firesphere\CSPHeaders\Helpers\CSPConvertor
footer: false
---

# CSPConvertor

Class \Firesphere\CSPHeaders\Helpers\CSPConvertor



* Full name: `\Firesphere\CSPHeaders\Helpers\CSPConvertor`



## Methods

### toYml



```php
public static CSPConvertor::toYml(\SilverStripe\Control\HTTPResponse $response, mixed $return = false): string|void
```



* This method is **static**.




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `response` | **\SilverStripe\Control\HTTPResponse** |  |
| `return` | **mixed** |  |


**Return Value:**





---


---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)
