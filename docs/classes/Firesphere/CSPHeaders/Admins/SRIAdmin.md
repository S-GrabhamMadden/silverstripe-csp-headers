---
title: \Firesphere\CSPHeaders\Admins\SRIAdmin
footer: false
---

# SRIAdmin

Class \Firesphere\CSPHeaders\Admins\SRIAdmin



* Full name: `\Firesphere\CSPHeaders\Admins\SRIAdmin`
* Parent class: [ModelAdmin](../../../../classes.md)





---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)
