---
title: \Firesphere\CSPHeaders\Traits\CSPBackendTrait
footer: false
---

# CSPBackendTrait

Trait Firesphere\CSPHeaders\Traits\CSPBackendTrait

Trait CSPBackendTrait contains all variables, static and dynamic, and their respective getters and setters
This is to keep the CSPBackend class itself more readable

* Full name: `\Firesphere\CSPHeaders\Traits\CSPBackendTrait`




## Methods

### isJsSRI



```php
public static CSPBackendTrait::isJsSRI(): bool
```



* This method is **static**.





**Return Value:**





---
### setJsSRI



```php
public static CSPBackendTrait::setJsSRI(bool $jsSRI): void
```



* This method is **static**.




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `jsSRI` | **bool** |  |


**Return Value:**





---
### isCssSRI



```php
public static CSPBackendTrait::isCssSRI(): bool
```



* This method is **static**.





**Return Value:**





---
### setCssSRI



```php
public static CSPBackendTrait::setCssSRI(bool $cssSRI): void
```



* This method is **static**.




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `cssSRI` | **bool** |  |


**Return Value:**





---
### getHeadCSS



```php
public static CSPBackendTrait::getHeadCSS(): array
```



* This method is **static**.





**Return Value:**





---
### setHeadCSS



```php
public static CSPBackendTrait::setHeadCSS(array $headCSS): void
```



* This method is **static**.




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `headCSS` | **array** |  |


**Return Value:**





---
### getHeadJS



```php
public static CSPBackendTrait::getHeadJS(): array
```



* This method is **static**.





**Return Value:**





---
### setHeadJS



```php
public static CSPBackendTrait::setHeadJS(array $headJS): void
```



* This method is **static**.




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `headJS` | **array** |  |


**Return Value:**





---
### isUsesNonce



```php
public static CSPBackendTrait::isUsesNonce(): bool
```



* This method is **static**.





**Return Value:**





---
### setUsesNonce



```php
public static CSPBackendTrait::setUsesNonce(bool $useNonce): void
```



* This method is **static**.




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `useNonce` | **bool** | static::isUseNonce() |


**Return Value:**





---
### getJsBuilder



```php
public CSPBackendTrait::getJsBuilder(): \Firesphere\CSPHeaders\Builders\JSBuilder
```









**Return Value:**





---
### setJsBuilder



```php
public CSPBackendTrait::setJsBuilder(\Firesphere\CSPHeaders\Builders\JSBuilder $jsBuilder): void
```








**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `jsBuilder` | **\Firesphere\CSPHeaders\Builders\JSBuilder** |  |


**Return Value:**





---
### getCssBuilder



```php
public CSPBackendTrait::getCssBuilder(): \Firesphere\CSPHeaders\Builders\CSSBuilder
```









**Return Value:**





---
### setCssBuilder



```php
public CSPBackendTrait::setCssBuilder(\Firesphere\CSPHeaders\Builders\CSSBuilder $cssBuilder): void
```








**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `cssBuilder` | **\Firesphere\CSPHeaders\Builders\CSSBuilder** |  |


**Return Value:**





---

---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)

