---
title: \Firesphere\CSPHeaders\Tasks\SRIRefreshTask
footer: false
---

# SRIRefreshTask

Class Firesphere\CSPHeaders\Tasks\SRIRefreshTask



* Full name: `\Firesphere\CSPHeaders\Tasks\SRIRefreshTask`
* Parent class: [BuildTask](../../../../classes.md)



## Methods

### run

Deletes the Sub-resource integrity values on build of the database
so they're regenerated next time that file is required.

```php
public SRIRefreshTask::run(mixed $request): mixed
```








**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `request` | **mixed** |  |


**Return Value:**





---


---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)
