---
title: \Firesphere\CSPHeaders\Builders\SRIBuilder
footer: false
---

# SRIBuilder

Class Firesphere\CSPHeaders\Builders\SRIBuilder



* Full name: `\Firesphere\CSPHeaders\Builders\SRIBuilder`



## Methods

### buildSRI



```php
public SRIBuilder::buildSRI( $file, array $htmlAttributes): array
```








**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `file` | **** |  |
| `htmlAttributes` | **array** |  |


**Return Value:**





---
### shouldUpdateSRI



```php
private SRIBuilder::shouldUpdateSRI(): bool
```









**Return Value:**





---


---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)
